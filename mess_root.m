function mess_root = mess_root()
%% Return root path of loaded M-M.E.S.S.

%
% This file is part of the M-M.E.S.S. project
% (http://www.mpi-magdeburg.mpg.de/projects/mess).
% Copyright (c) 2009-2025 Jens Saak, Martin Koehler, Peter Benner and others.
% All rights reserved.
% License: BSD 2-Clause License (see COPYING)
%

[mess_root, ~, ~] = fileparts(mfilename('fullpath'));
