# M-M.E.S.S. - The Matrix Equation Sparse Solver Library for MATLAB and Octave

## General

please refer to the M-M.E.S.S. toolbox by its acronym and the version number.
The core copyright holders are Jens Saak, Martin Köhler and Peter Benner.
They have to be given as the authors in a citation. The project is frequently
updated on Zenodo and every stable release will receive a DOI for proper
citation there. The DOI for this version and a sample BibTeX entry can be found
below.

## DOI

The DOI for version 3.1 is
[10.5281/zenodo.14929081](http://doi.org/10.5281/zenodo.14929081)

## BibTeX

```
@Misc{SaaKB21-mmess-3.1,
  author =       {Saak, J. and K\"{o}hler, M. and Benner, P.},
  title =        {{M-M.E.S.S.}-3.1 -- The Matrix Equations Sparse Solvers
                  library},
  month =        feb,
  year =         2025,
  note =         {see also:\url{https://www.mpi-magdeburg.mpg.de/projects/mess}},
  doi =          {10.5281/zenodo.14929081},
  key =          {MMESS}
}

```
