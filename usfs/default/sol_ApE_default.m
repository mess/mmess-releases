function X = sol_ApE_default(eqn, opts, opA, p, opE, C, opC)
% function X=sol_ApE_default(eqn, opts,opA,p,opE,C,opC)
%
% This function returns X = (A_ + p*E_)\C, where matrices A_ and E_
% given by structure eqn and input matrix C could be transposed.
% Matrices A_ and E_ are assumed to be quadratic.
%
%   Inputs:
%
%   eqn     structure containing fields 'A_' and 'E_'
%   opts    structure containing parameters for the algorithm
%   opA     character specifying the shape of A
%           opA = 'N' solves (A_ + p* opE(E_))*X = opC(C)
%           opA = 'T' solves (A_' + p* opE(E_))*X = opC(C)
%   p       scalar value
%   opE     character specifying the shape of E_
%           opE = 'N' solves (opA(A_) + p* E_)*X = opC(C)
%           opE = 'T' solves (opA(A_) + p* E_')*X = opC(C)
%   C       n-x-p matrix
%   opC     character specifies the form of opC(C)
%           opC = 'N' solves (opA(A_) + p* opE(E_))*X = C
%           opC = 'T' solves (opA(A_) + p* opE(E_))*X = C'
%
%   Output:
%
%   X       matrix fulfilling equation (opA(A_)+p*opE(E_))*X = opC(C)
%
% This function does not use other default functions.

%
% This file is part of the M-M.E.S.S. project
% (http://www.mpi-magdeburg.mpg.de/projects/mess).
% Copyright (c) 2009-2025 Jens Saak, Martin Koehler, Peter Benner and others.
% All rights reserved.
% License: BSD 2-Clause License (see COPYING)
%

%% check input parameters
if not(ischar(opA)) || not(ischar(opE)) || not(ischar(opC))
    mess_err(opts, 'error_arguments', 'opA, opE or opC is not a char');
end

opA = upper(opA);
opE = upper(opE);
opC = upper(opC);

if not(opA == 'N' || opA == 'T')
    mess_err(opts, 'error_arguments', 'opA is not ''N'' or ''T''');
end

if not(opE == 'N' || opE == 'T')
    mess_err(opts, 'error_arguments', 'opE is not ''N'' or ''T''');
end

if not(opC == 'N' || opC == 'T')
    mess_err(opts, 'error_arguments', 'opC is not ''N'' or ''T''');
end

if not(isnumeric(p)) || not(length(p) == 1)
    mess_err(opts, 'error_arguments', 'p is not numeric');
end

if not(isnumeric(C)) || not(ismatrix(C))
    mess_err(opts, 'error_arguments', 'C has to ba a matrix');
end

%% check data in eqn structure
if not(isfield(eqn, 'haveE'))
    eqn.haveE = false;
end
if eqn.haveE
    if not(isfield(eqn, 'E_')) || not(isfield(eqn, 'A_'))
        mess_err(opts, 'error_arguments', 'field eqn.E_ or eqn.A_ is not defined');
    end
else
    if not(isfield(eqn, 'A_'))
        mess_err(opts, 'error_arguments', 'field eqn.A_ is not defined');
    end
end

[rowA, colA] = size(eqn.A_);

switch opA

    case 'N'

        switch opE

            case 'N'

                switch opC

                    % implement solve (A_+p*E_)*X=C
                    case 'N'

                        if not(rowA == size(C, 1))
                            mess_err(opts, 'error_arguments', ['number ' ...
                                                               'of rows of A differs ' ...
                                                               'with number of rows of C']);
                        end

                        X = (eqn.A_ + p * eqn.E_) \ C;

                        % implement solve (A_ + p * E_) * X = C'
                    case 'T'

                        if not(rowA == size(C, 2))
                            mess_err(opts, 'error_arguments', ['number ' ...
                                                               'of rows of A differs ' ...
                                                               'with number of ' ...
                                                               'columns of C']);
                        end

                        X = (eqn.A_ + p * eqn.E_) \ C';

                end

            case 'T'

                switch opC

                    % implement solve (A_ + p * E_') * X = C
                    case 'N'

                        if not(rowA == size(C, 1))
                            mess_err(opts, 'error_arguments', ['number ' ...
                                                               'of rows of A differs ' ...
                                                               'with number of rows of C']);
                        end

                        X = (eqn.A_ + p * eqn.E_') \ C;

                        % implement solve (A_ + p * E_') * X = C'
                    case 'T'

                        if not(rowA == size(C, 2))
                            mess_err(opts, 'error_arguments', ['number ' ...
                                                               'of rows of A differs ' ...
                                                               'with number of ' ...
                                                               'columns of C']);
                        end

                        X = (eqn.A_ + p * eqn.E_') \ C';

                end

        end

    case 'T'

        switch opE

            case 'N'

                switch opC

                    % implement solve (A_' + p * E_) * X = C
                    case 'N'

                        if not(colA == size(C, 1))
                            mess_err(opts, 'error_arguments', ['number ' ...
                                                               'of columns of A ' ...
                                                               'differs with number ' ...
                                                               'of rows of C']);
                        end

                        X = (eqn.A_' + p * eqn.E_) \ C;

                        % implement solve (A_' + p * E_) * X = C'
                    case 'T'

                        if not(colA == size(C, 2))
                            mess_err(opts, 'error_arguments', ['number ' ...
                                                               'of columns of A ' ...
                                                               'differs with number ' ...
                                                               'of columns of C']);
                        end

                        X = (eqn.A_' + p * eqn.E_) \ C';

                end

            case 'T'

                switch opC

                    % implement solve (A_' + p * E_') * X = C
                    case 'N'

                        if not(colA == size(C, 1))
                            mess_err(opts, 'error_arguments', ['number ' ...
                                                               'of columns of A ' ...
                                                               'differs with number ' ...
                                                               'of rows of C']);
                        end

                        X = (eqn.A_' + p * eqn.E_') \ C;

                        % implement solve (A_' + p * E_') * X = C'
                    case 'T'

                        if not(colA == size(C, 2))
                            mess_err(opts, 'error_arguments', ['number ' ...
                                                               'of columns of A ' ...
                                                               'differs with number ' ...
                                                               'of columns of C']);
                        end

                        X = (eqn.A_' + p * eqn.E_') \ C';

                end
        end

end
end
