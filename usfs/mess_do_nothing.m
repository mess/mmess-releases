function varargout = mess_do_nothing(varargin)
%% function vargin = mess_do_nothing(vargin)
%
% This function is a placeholder for unset function handles.
%
% Output = Input

%
% This file is part of the M-M.E.S.S. project
% (http://www.mpi-magdeburg.mpg.de/projects/mess).
% Copyright (c) 2009-2025 Jens Saak, Martin Koehler, Peter Benner and others.
% All rights reserved.
% License: BSD 2-Clause License (see COPYING)
%

varargout = varargin;
