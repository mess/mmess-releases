function [eqn, opts, oper] = ...
    mul_A_post_state_space_transformed_default(eqn, opts, oper)
%% function [eqn, opts, oper] = ...
%     mul_A_post_state_space_transformed_default(eqn, opts, oper)
%
% function post finalizes data and/or functions
%
% Input
%   eqn             struct contains data for equations
%
%   opts            struct contains parameters for the algorithm
%
%   oper            struct contains function handles for operation
%                   with A and E
%
% Output
%   eqn             struct contains data for equations
%
%   opts            struct contains parameters for the algorithm
%
%   oper            struct contains function handles for operation
%                   with A and E

%
% This file is part of the M-M.E.S.S. project
% (http://www.mpi-magdeburg.mpg.de/projects/mess).
% Copyright (c) 2009-2025 Jens Saak, Martin Koehler, Peter Benner and others.
% All rights reserved.
% License: BSD 2-Clause License (see COPYING)
%

eqn = LU_E_clean(eqn, opts);
